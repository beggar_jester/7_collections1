public class Collections1 {
    public static void main(String[] args) {
        CustomArrayList<Integer> first = new CustomArrayList<>();
        System.out.println("Check the arrayList empty:");
        System.out.println(first.isEmpty());
        first.add(10);
        first.add(9);
        first.add(11);
        first.add(17);
        System.out.println("Add some elements & check arrayList size:");
        System.out.println(first.size());
        System.out.println("Print our arrayList:");
        System.out.println(first);
        System.out.println("Proof that contains checking works correctly:");
        System.out.print("ArrayList contains 1? ");
        System.out.println(first.contains(1));
        System.out.print("ArrayList contains 10? ");
        System.out.println(first.contains(10));
        System.out.println("Remove element by object:");
        first.remove(Integer.valueOf(11));
        System.out.println(first);
        System.out.println("Remove element by index:");
        first.remove(0);
        System.out.println(first);
        System.out.println("Get second element: " + first.get(1));
        System.out.println("Clear the arrayList and print it:");
        first.clear();
        System.out.println(first);

        System.out.println();

        CustomArrayList<String> second = new CustomArrayList<>();
        System.out.println("Check the arrayList empty:");
        System.out.println(second.isEmpty());
        second.add("a");
        second.add("b");
        second.add("c");
        second.add("d");
        System.out.println("Add some elements & check arrayList size:");
        System.out.println(second.size());
        System.out.println("Print our arrayList:");
        System.out.println(second);
        System.out.println("Proof that contains checking works correctly:");
        System.out.print("ArrayList contains 1? ");
        System.out.println(second.contains("z"));
        System.out.print("ArrayList contains 10? ");
        System.out.println(second.contains("c"));
        System.out.println("Remove element by object:");
        second.remove("d");
        System.out.println(second);
        System.out.println("Remove element by index:");
        second.remove(0);
        System.out.println(second);
        System.out.println("Get second element: " + second.get(1));
        System.out.println("Clear the arrayList and print it:");
        second.clear();
        System.out.println(second);
    }
}
