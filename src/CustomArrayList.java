import java.util.Arrays;

public class CustomArrayList<typeName> {
    private static final int Capacity = 10;
    private int arrayListSize = 0;
    private typeName[] arrayListElements;

    public CustomArrayList() {
        arrayListElements = (typeName[]) new Object[Capacity];
    }

    public void add(typeName newElement) {
        if (arrayListSize == arrayListElements.length) ensureCapacity();
        arrayListElements[arrayListSize++] = newElement;
    }

    public typeName remove(int index) {
        if (index < 0 || index >= arrayListSize) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size " + arrayListSize);
        }

        typeName elementToRemove = arrayListElements[index];
        if (arrayListSize - 1 - index >= 0)
            System.arraycopy(arrayListElements, index + 1, arrayListElements, index, arrayListSize - 1 - index);
        arrayListSize--;
        return elementToRemove;
    }

    public typeName remove(typeName elementToRemove) {
        int indexOfElement = -1;
        for (int i = 0; i < arrayListElements.length; i++)
            if (arrayListElements[i] == elementToRemove) indexOfElement = i;
        if (indexOfElement == -1) throw new IllegalArgumentException(elementToRemove + " element does not exists.");
        return remove(indexOfElement);
    }

    public boolean contains(typeName elementToFind) {
        return Arrays.stream(arrayListElements).anyMatch(element -> element == elementToFind);
    }

    public int size() {
        return arrayListSize;
    }

    public boolean isEmpty() {
        return arrayListSize == 0;
    }

    public typeName get(int index) {
        if (index < 0 || index >= arrayListSize) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size " + arrayListSize);
        }

        return arrayListElements[index];
    }

    public void clear() {
        while (arrayListSize > 0) remove(arrayListSize - 1);
    }

    private void ensureCapacity() {
        arrayListElements = Arrays.copyOf(arrayListElements, arrayListElements.length * 2);
    }

    @Override
    public String toString() {
        StringBuilder answer = new StringBuilder("CustomArrayList{ ");
        for (int i = 0; i < arrayListSize; i++) {
            answer.append(arrayListElements[i]).append(" ");
        }
        answer.append('}');
        return answer.toString();
    }
}
